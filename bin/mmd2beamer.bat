@echo off
multimarkdown -b -t beamer "%1"
call latexmk -xelatex -latexoption="--shell-escape --interaction=nonstopmode" "%~n1.tex"
call latexmk -c
