# 개요

`simplekorean`에서 \$ 기호로 수식을 표현하고도 잘 출력되게 하는 것이다.


# 예문

테스트를 위한 예문이다.

@@@

복소수 $z$가 다음과 같다고 생각하자.
$$
z=\cos x+ i \sin x
$$
양변을 $x$에 대해 미분하면, $\frac{dz}{dx}= -\sin x + i \cos x$가 된다. $i^2=-1$이므로 
$$
\frac{dz}{dx}= i^2\sin x+ i \cos x= i (\cos x + i \sin x) = iz.
$$
양변을 정리하여 적분하면
$$
\begin{aligned}
\frac1z \frac{dz}{dx} &= i \\
\int \frac1z dz &= \int i dx \\
\ln z &= ix + C.
\end{aligned}
$$
$C$는 적분상수이다. $x=0$일 때 $\ln z= C$이고 $z\,\vert_{x=0}=\cos 0+ i \sin 0 = 1$이므로 $C= \ln 1 = 0$이다. 따라서, $\ln z = i x$이고 $z=e^{ix}$이므로,
$$
e^{ix} = \cos x + i \sin x
$$
이다. 

