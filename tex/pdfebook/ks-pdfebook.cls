%%
%% ks-pdfebook.cls
%%	ver 2022.0103.ksminitex
%%  ver 2024.0529
%%
%% 2022.0103: TL 2020 xetex had color-bug. Just change the footnote fore-color.
%% 2024.0529: (1) title and author can have multi lines (2) backgroundimage 
%%
\ProvidesClass{ks-pdfebook}[2022/01/03 v2024.0529]

\LoadClass[10.5pt,chapter,openany]{oblivoir}

\hypersetup{hidelinks}

\SetHangulspace{1.5}{1.08}

\RequirePackage{fapapersize}
\setfootins{20pt}{\bigskipamount}
\usefapapersize{120mm,160mm,10mm,*,10mm,17mm}

\RequirePackage{amsmath,amssymb,amsthm}

\RequirePackage{graphicx}
\RequirePackage{xcolor,ninecolors}
\RequirePackage{efbox}
\RequirePackage{relsize}
%\RequirePackage[contents={}]{background}
\RequirePackage{wallpaper}

%%%
\hypersetup{pdfcreator={txt2pdfebook powered by XeTeX}}

%\setmainfont{RIDIBatang}
%\setmainfont{TeX Gyre Pagella}
\setobmainfont[texgyrepagella-](regular.otf)(bold.otf)(italic.otf)
\setkomainfont(RIDIBatang)(KoPubBatang Bold)(KoPubDotum Bold)[](KoPubBatang Medium)
\setkosansfont(KoPubDotum Light)(KoPubDotum Bold)
%\setkomonofont(D2Coding.ttf)
\setkomonofont(UnTaza.ttf)
\newfontfamily\fallbackhanjafont{Noto Serif CJK KR}[Scale=.95]

\newif\ifmaketoc

\RequirePackage{pagecolor}
\RequirePackage{tikz}

%%% themes
\ExplSyntaxOn 

\keys_define:nn { pdfebook }
{
	pagecolor	.tl_set:N = \optpagecolor,
	textcolor	.tl_set:N = \opttextcolor,
	bgcolor	.tl_set:N = \optbgcolor,
	fgcolor	.tl_set:N = \optfgcolor,
	linkcolor	.tl_set:N = \optlinkcolor,
	rulecolor	.tl_set:N = \optrulecolor,
	titlecolor	.tl_set:N = \opttitlecolor,
	authorcolor	.tl_set:N = \optauthorcolor,
	datecolor	.tl_set:N = \optdatecolor,
	headercolor	.tl_set:N = \optheadercolor,
	foliocolor	.tl_set:N = \optfoliocolor,
	sectionboxcolor	.tl_set:N = \optsectionboxcolor,
	chapterheadercolor .tl_set:N = \optchapterheadercolor,
	progressbarcolor .tl_set:N = \optprogressbarcolor
}

\NewDocumentCommand \setThemeDefault { }
{
    \keys_set:nn { pdfebook }
    {
    	pagecolor = white,
    	textcolor = black,
    	bgcolor = gray!50,
    	fgcolor	= white,
    	linkcolor = olive5,
    	rulecolor = teal3,
    	foliocolor = olive6,
    	titlecolor = black!80,
    	authorcolor = teal7,
    	datecolor = teal6,
    	headercolor = teal1,
    	sectionboxcolor = cyan!50,
    	chapterheadercolor = black!90,
		progressbarcolor = red,
    }
}

\NewDocumentCommand \setThemeDark {}
{
    \keys_set:nn { pdfebook }
    {
    	pagecolor = black!80,
    	textcolor = white,
    	bgcolor = gray,
    	fgcolor	= green,
    	linkcolor = olive6,
    	rulecolor = teal8,
    	foliocolor = olive8,
    	titlecolor = white,
    	authorcolor = gray!10,
    	datecolor = gray!15,
    	headercolor = teal9,
    	sectionboxcolor = cyan!50,
    	chapterheadercolor = gray!5,
		progressbarcolor = yellow!60!red,
    }
}

\AtBeginDocument{
	\flushbottom
	\pagecolor{\optpagecolor}
	\color{\opttextcolor}
%%% TL 2020 hacks
  \pretocmd{\@makefntext}{\color{\opttextcolor}}{}{}
}

\int_new:N \l_lpage_int

\makepagestyle{soseoltoc}
\makeoddhead{soseoltoc}{}{}{}
\makeoddfoot{soseoltoc}{%
  }%
  {\color{\optrulecolor}\hrule}%
  {\null\\[2mm] \footnotesize \color{\optfoliocolor}\thepage/\thelastpage}
\makeevenhead{soseoltoc}{}{}{}
\makeevenfoot{soseoltoc}{%
  }%
  {\color{\optrulecolor}\hrule}%
  {\null\\[2mm] \footnotesize \color{\optfoliocolor}\thepage/\thelastpage}

\makepagestyle{soseol}
\makeoddhead{soseol}{}{}{}
\makeoddfoot{soseol}{%
    \int_compare:nTF { \thelastpage == 0 }
    {
    	\int_set:Nn \l_lpage_int { 1000 }
    }
    {
    	\int_set:Nn \l_lpage_int { \value { lastpage } }
    }
%	\fp_set:Nn \l_tmpa_fp { \value{page} / \l_lpage_int }
	\fp_set:Nn \l_tmpa_fp { ( \value{page} - 1 + \value{page} /\l_lpage_int )  / \l_lpage_int }

  \ifmaketoc
  	\tikz [overlay] \draw[\optprogressbarcolor] (\fp_use:N \l_tmpa_fp \textwidth ,-.39cm) -- (\fp_use:N \l_tmpa_fp \textwidth,-1.1cm);
	\null\\[2mm] \footnotesize \color{\optlinkcolor}\hyperlink{toc:toc}{\efbox[backgroundcolor=\optbgcolor]{\color{\optfgcolor}TOC}}%
  \else
  	\tikz [overlay] \draw[\optprogressbarcolor] (\fp_use:N \l_tmpa_fp \textwidth ,.3cm) -- (\fp_use:N \l_tmpa_fp \textwidth,-1.5cm);
  \fi
  }%
  {\color{\optrulecolor}\hrule}%
  {\null\\[2mm] \footnotesize \color{\optfoliocolor}\thepage/\thelastpage}
\makeevenhead{soseol}{}{}{}
\makeevenfoot{soseol}{%
    \int_compare:nTF { \thelastpage == 0 }
    {
    	\int_set:Nn \l_lpage_int { 1000 }
    }
    {
    	\int_set:Nn \l_lpage_int { \value { lastpage } }
    }
	\fp_set:Nn \l_tmpa_fp { ( \value{page} - 1 + \value{page} /\l_lpage_int )  / \l_lpage_int }

  \ifmaketoc
  	\tikz [overlay] \draw[\optprogressbarcolor] (\fp_use:N \l_tmpa_fp \textwidth ,-.39cm) -- (\fp_use:N \l_tmpa_fp \textwidth,-1.1cm);
	\null\\[2mm] \footnotesize \color{\optlinkcolor}\hyperlink{toc:toc}{\efbox[backgroundcolor=\optbgcolor]{\color{\optfgcolor}TOC}}%
  \else
  	\tikz [overlay] \draw[\optprogressbarcolor] (\fp_use:N \l_tmpa_fp \textwidth ,.3cm) -- (\fp_use:N \l_tmpa_fp \textwidth,-1.5cm);
  \fi
  }%
  {\color{\optrulecolor}\hrule}%
  {\null\\[2mm] \footnotesize \color{\optfoliocolor}\thepage/\thelastpage}

\ExplSyntaxOff

\parindent=1em
\parskip=2pt plus 1pt minus .5pt

\AtBeginDocument{
\clubpenalty=0\widowpenalty=150
%\pagestyle{soseol}
\chapterstyle{obbringhurst}
}

\ExplSyntaxOn 

\NewDocumentCommand \MakeTitlePages {  }
{
	\seq_set_split:NnV \l_tmpa_seq { ; } \mytitle
	\seq_set_split:NnV \l_tmpb_seq { ; } \myauthor
	\titlingpageend{\clearpage}{\clearpage}
	\tl_if_exist:NT \titlepageimage
	{
%		\backgroundsetup{contents={\includegraphics[width=paperwidth,height=paperheight]{\titlepageimage}}}
%		\backgroundsetup{contents={\includegraphics[width=\paperwidth,height=\paperheight]{\titlepageimage}}}
%		\BgThispage
		\ThisCenterWallPaper{1}{\titlepageimage}
	}
	\begin{titlingpage*}
	\null
	\vspace{.3\textheight}
	\seq_map_indexed_inline:Nn \l_tmpa_seq
	{
		\centerline{\LARGE\color{\opttitlecolor}\hangulfontspec{KoPub~Dotum~Medium.ttf}##2}
		\int_compare:nT { ##1 < \seq_count:N \l_tmpa_seq }
		{ \par }
	}
		
	\bigskip
%	\centerline{\large\color{\optauthorcolor}\myauthor}
	\seq_map_indexed_inline:Nn \l_tmpb_seq
	{
		\centerline{\Large\color{\optauthorcolor}\hangulfontspec{KoPub~Dotum~Medium.ttf}##2}
		\int_compare:nT { ##1 < \seq_count:N \l_tmpa_seq }
		{ \par }
	}
	
	\ifx\mydate\undefined\else
		\bigskip
		\centerline{\normalsize\selectfont \color{\optdatecolor}\mydate}
	\fi
	\title{\mytitle}
	\author{\myauthor}
	\ifx\mydate\undefined\else\date{\mydate}\fi
	\end{titlingpage*}
%	\NoBgThispage
}

\makechapterstyle{obbringhurst}{%
  \chapterstyle{default}
  \renewcommand*{\chapterheadstart}{}
%  \renewcommand*{\printchaptername}{}
  \renewcommand*{\prechapternum}{}
  \renewcommand*{\postchapternum}{}
  \renewcommand*{\hchaptertitlehead}{}
  \renewcommand*{\chapternamenum}{}
  \renewcommand*{\printchapternum}{}
  \renewcommand*{\afterchapternum}{}
  \renewcommand*{\printchaptertitle}[1]{%
%%%    \raggedright\Large\scshape\MakeLowercase{##1}}
    \color{\optchapterheadercolor}\memRTLraggedright\Large\sffamily\bfseries{##1}}
  \renewcommand*{\afterchaptertitle}{%
  \vskip\onelineskip \hrule\vskip\onelineskip}}

\ExplSyntaxOff

\maxtocdepth{section}
\counterwithout{section}{chapter}
\counterwithin*{section}{chapter}

\setsecheadstyle{%
	\leavevmode\llap{\color{\optsectionboxcolor}\rule{7pt}{7pt}\space}%
	\color{\optheadercolor}\sffamily\normalsize\selectfont}

\setparaheadstyle{%
	\noindent\sffamily\bfseries\color{\optheadercolor}}

\let\subsection\paragraph

%\hangsecnum

\renewcommand\cftsectionfont{\small}
\renewcommand\cftsectionindent{4em}

\renewcommand\clearforchapter{\clearpage}
\renewcommand\cleardoublepage{\clearpage}
\renewcommand\cleartorecto{\clearpage}

\renewcommand\cftchapterdotsep{}
\renewcommand\cftchapterleader{\hfil}
\renewcommand\cftchapteraftersnum{\hfil}
\renewcommand\cftchapterafterpnum{\par}
\renewcommand\cftsectiondotsep{\cftnodots}
\renewcommand\cftsectionleader{\hfil}
%\renewcommand\cftsectionaftersnum{\hfil}
\renewcommand\cftsectionafterpnum{\hskip\parfillskip}

\aliaspagestyle{chapter}{soseol}

\newcommand{\newlistoflink}[4]{%
  \@namedef{ext@#2}{#2}
  \@ifundefined{c@#2depth}{\newcounter{#2depth}}{}
  \setcounter{#2depth}{1}
  \@namedef{#2mark}{\markboth{#3}{#3}}
   \@namedef{#1}{\@ifstar{\@nameuse{mem@#1}{01}}{\@nameuse{mem@#1}{00}}}
  \@namedef{cft#2beforelisthook}{}%
  \@namedef{cft#2afterlisthook}{}%
  \@namedef{mem@#1}##1{%
    \ensureonecol
    \par
    \begingroup
      \phantomsection
      \if##1
        \ifmem@em@starred@listof\else
          \addcontentsline{toc}{chapter}{#3}
        \fi
      \fi
      \@nameuse{@#2maketitle}
      \hypertarget{#4}{}
      \parskip\cftparskip
      \@nameuse{cft#2beforelisthook}%
      \@starttoc{#2}%
      \@nameuse{cft#2afterlisthook}%
    \endgroup
    \restorefromonecol}
  \@namedef{@#2maketitle}{%
    \@nameuse{#2headstart}
   {\parindent\z@
    \parskip\z@
%%%%  \parskip\cftparskip
    \interlinepenalty\@M
    \@nameuse{print#2nonum}%
    \@nameuse{print#2title}{#3}%
    \@nameuse{#2mark}%
    \thispagestyle{soseoltoc}%
    \@nameuse{after#2title}
   }
    \@afterheading}
  \@namedef{#2headstart}{\chapterheadstart}
  \@namedef{after#2title}{\afterchaptertitle}
  \@namedef{print#2nonum}{\printchapternonum}
  \@namedef{print#2title}##1{\printchaptertitle{##1}}
} % end \newlistof

\newlistoflink{tableofcontents}{toc}{\contentsname}{toc:toc}

\ifx\oblivoirlist\undefined
	\firmlists
\else
	\oblivoirlists
\fi

%%%
%%% the following patch is for TL2020's tikz.
%%%
\ExplSyntaxOn
\cs_if_exist:NT \tikz@ensure@dollar@catcode
{
	\cs_set_eq:NN \tikz@ensure@dollar@catcode \scan_stop:
}

\ExplSyntaxOff
\endinput
