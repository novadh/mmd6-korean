%!TEX encoding = UTF-8
%%
%% file `mynovel.cls`
%%
%% (C) 2013-2021 Nova De Hi, all rights reserved.
%%
%% version 1.0.7, 2013-09-16
%%         1.0.8, 2013-09-26
%%         1.0.9, 2013-10-09
%%         1.0.10, 2013-12-20
%%	   1.0.11, 2014-01-08
%%	   1.0.12, 2015-07-10
%%	   1.0.13, 2015-07-26
%%		1.0.14, 2020-01-03
%%		1.1, 2021-12-03
%% 
%% options:
%%	[smallpage] : trim margins: deprecated
%%	[attachchapcnt] : Attach chapter counter
%%  [subsecreset] : reset subset counter every section
%% commands:
%%  \MakeTitlePages[color]{title}{subtitle}{author}
%%  
\ProvidesClass{ks-mynovel}[2013/10/09]

\newif\ifSmallPG\SmallPGfalse
\DeclareOption{smallpage}{\SmallPGtrue}
\newif\ifAttachChapCNT\AttachChapCNTfalse
\DeclareOption{attachchapcnt}{\AttachChapCNTtrue}
\newif\ifSubsecReset\SubsecResetfalse
\DeclareOption{subsecreset}{\SubsecResettrue}
\newif\ifParagraphCmd\ParagraphCmdfalse
\DeclareOption{paracmd}{\ParagraphCmdtrue}
\ProcessOptions

\LoadClass[b5paper,12pt,twoside,itemph]{oblivoir}

\RequirePackage{fapapersize}

%\setheadfoot{10mm}{20mm}

%\ifSmallPG
%  \usefapapersize{120mm,190mm,5mm,*,5mm,7mm}
%\else
%%% b5=176x250
\usefapapersize{*,*,35mm,*,35mm,*}
%\fi

\SetHangulspace{1.38}{1.1}

\RequirePackage[svgnames,x11names]{xcolor}

\maxsecnumdepth{chapter}
\maxtocdepth{subsection}

\RequirePackage{indentfirst}

%\usepackage{libertine}

%\ifXeTeX
%%	\defaultfontfeatures{Mapping=tex-text}
%	\else\ifLuaTeX
%%	\defaultfontfeatures{Ligatures=TeX}
%\fi\fi
\ifLuaOrXeTeX
%	\setmainfont[Mapping=tex-text,Ligatures=TeX]{NanumMyeongjo}
%	\setkormainfont{NanumMyeongjo}
%	\setmainhanjafont{HCR Batang LVT}
%	\setkorsansfont{NanumGothic}
	\setmainfont{XITS}
	\setkomainfont[KoPubBatang ](Light)(Medium)
	\setkomainfont[KoPubDotum ](Light)(Medium)
\fi
%\ifXeTeX
%	\xetexkofontregime[cjksymbols=latin]{hangul}
%\fi

\def\QUOTEfont{\small}

\let\ORIGquote=\quotation
\let\endORIGquote=\endquotation
\def\quotation{\ORIGquote\QUOTEfont\renewcommand\baselinestretch{1.2}}
\def\endquotation{\endORIGquote}
\let\quote=\quotation
\let\endquote=\endquotation

\newcommand*\SetQuoteFont[1]{%
	\def\QUOTEfont{#1}
}

\RequirePackage{kslinematters}

% \newcommand*{\newlinecommand}[1]{%
%   \newcommand*{#1}{%
%     \begingroup%
%     \lccode`\~=\endlinechar%
%     \lowercase{\def\restofline@aux####1~}{\endgroup\csname\string#1\endcsname{####1}\space}%
%     \catcode\endlinechar=\active%
%     \restofline@aux%
%   }%
%   \expandafter\def\csname\string#1\endcsname##1%
% }

%%% \dialog usage: get parameter to the end of line
%%% \dialog* : do not make paragraph
\def\dialog{%
	\@ifstar\@dialognoline\@dialogline
}

\newlinecommand\@dialognoline{%
		“\nobreak #1\nobreak”%
}

\newlinecommand\@dialogline{%
	\par
	“\nobreak #1\nobreak”%
	\par
}

\newcommand\myParen[1]{%
	\leavevmode ‘\nobreak #1\nobreak’%
}

\newcommand\myQuote[1]{%
	\leavevmode “\nobreak #1\nobreak”%
}

\makepagestyle{polarbear}
\makeoddhead{polarbear}{}{}{}
\makeevenhead{polarbear}{}{}{}
\makeoddfoot{polarbear}{}{}{\raisebox{-14pt}{{\color{gray}\sffamily\tiny\rightmark}\hspace{1em}{\color{blue}\small\rmfamily\bfseries\thepage}}}
\makeevenfoot{polarbear}{\raisebox{-14pt}{{\color{blue}\small\rmfamily\bfseries\thepage}\hspace{1em}{\color{gray}\sffamily\tiny\TitleStr}}}{}{}

\newcommand\TableOfContents{\tableofcontents*}

\def\Title#1{\def\TitleStr{#1}}
\def\Author#1{\def\AuthorStr{#1}}
\def\SubTitle#1{\def\SubTitleStr{#1}}

\RequirePackage{wallpaper}

\newcommand\MakeTitle{%
    \begin{titlingpage}
    \null\vfill
    \IfFileExists{img/\jobname-title.jpg}{\ThisCenterWallPaper{1}{img/\jobname-title}}{}
    \begin{center}
    	\ifUSERCOLOR \color{\USERCOLOR} \else\color{gray}\fi
        {\HUGE\sffamily\bfseries \TitleStr} \\[4ex]
        {\large\sffamily\SubTitleStr} \\[12ex]
        {\hfill \large \AuthorStr}
    \end{center}
    \vfill \null
    \end{titlingpage}
}

\newcounter{mysec}
\newif\ifMYSTAR\MYSTARfalse

\newcommand\MakeSectionTitle{%
	\let\ORIGfootnote=\footnote
	\def\footnote##1{}%
	\ifMYSTAR\else\stepcounter{mysec}\fi
	\cleartorecto
%	\ThisCenterWallPaper{1}{sectitle}
	\thispagestyle{empty}
	\null\vskip60pt
	\begin{flushright} 
%	\color{green}
	\ifMYSTAR \else \Large 제\themysec 장 \\ \fi
	\LARGE\TMPSecStr
	\end{flushright}
	\cleartorecto
	\let\footnote=\ORIGfootnote
}

\def\myFancyBreak{%
	\fancybreak{*\ \ \ \ \ \ *\ \ \ \ \ \ *}
}

\let\ORIGsection=\section
\def\section{%
	\@ifnextchar*\my@section\my@@section
}
\def\my@section*#1{%
	\MYSTARtrue
	\def\TMPSecStr{#1}%
	\ORIGsection{#1}
	\ifSubsecReset\setcounter{mysubsec}{0}\fi
}
\newcommand*\my@@section[2][\empty]{%
	\MYSTARfalse
	\def\TMPSecStr{#2}%
	\ifx#1\empty
		\ifAttachChapCNT
			\ORIGsection{제\themysec 장\ \ \ #2}
		\else
			\ORIGsection{#2}
		\fi
	\else
		\ifAttachChapCNT
		    \ORIGsection[제\themysec 장\ \ \ #1]{제\themysec 장\ \ \ #2}
		\else
			\ORIGsection[#1]{#2}
		\fi
	\fi
   	\ifSubsecReset\setcounter{mysubsec}{0}\fi
}

\ifParagraphCmd
	\def\paragraph{\section*}
\fi

\setsechook{\MakeSectionTitle}
\setaftersecskip{15.1\onelineskip plus .5\onelineskip minus .3\onelineskip}
\setsubsecheadstyle{\color{NavyBlue}\Large\sffamily\bfseries}
\setbeforesubsecskip{3.5\onelineskip plus .5\onelineskip minus .3\onelineskip}
\setaftersubsecskip{1.6\onelineskip plus .5\onelineskip minus .3\onelineskip}

\copypagestyle{chapter}{empty}
\AtBeginDocument{\catcode`\~=12}

\newif\ifUSERCOLOR\USERCOLORfalse
\newcommand\MakeTitlePages[4][\empty]{%
	\ifx#1\empty \else
		\USERCOLORtrue
		\def\USERCOLOR{#1}
	\fi
	\Title{#2}
	\SubTitle{#3}
	\Author{#4}
	\MakeTitle
	\pagestyle{empty}
	\TableOfContents
	\cleartorecto
	\pagestyle{polarbear}
}

\let\ORIG@title=\title
\let\ORIG@author=\author
\let\ORIG@date=\date
\let\ORIG@maketitle=\maketitle
\def\title#1{\gdef\@TitleStr{#1}}
\def\author#1{\gdef\@AuthorStr{#1}}
\def\date#1{\gdef\@SubTitleStr{#1}}
\def\maketitle{%
	\MakeTitlePages{\@TitleStr}{\@SubTitleStr}{\@AuthorStr}
}

%%% ldots
\ifPDFTeX
	\let\ldots=\cntrdots
\fi

%%% 등장인물
\newcommand\PP[2]{%
    \par
    \hangpara{4em}{1}
    {\sffamily #1\ }%
    #2
    \bigskip
}

\def\BIGskip{\@ifnextchar*\myBIGskipA\myBIGskipB}
\def\myBIGskipA*{\myFancyBreak}
\def\myBIGskipB{\vspace{1.6\onelineskip}}
\let\ORIGbigskip=\bigskip
\let\bigskip=\BIGskip

\newcounter{mysubsec}
\let\ORIG@subsection=\subsection

\def\mysubsec{%
	\@ifnextchar*\mysubsec@star\mysubsec@nostar
}

\def\mysubsec@star#1[#2]{%
	\ORIG@subsection{#2}
}

\def\mysubsec@nostar{%
	\@ifnextchar[\mysubsec@arg\mysubsec@noarg
}
\def\mysubsec@arg[#1]{%
	\stepcounter{mysubsec}
	\ORIG@subsection{\#\themysubsec\ #1}
}
\def\mysubsec@noarg{%
	\stepcounter{mysubsec}
	\ORIG@subsection{\#\themysubsec}
}

\newcommand*\SectionCounter[2]{%
	\setcounter{mysec}{#1}
	\setcounter{mysubsec}{#2}
}

%\let\subsection=\mysubsec
\def\subsection{\@ifnextchar\bgroup\subsec@@arg\subsec@@noarg}
\def\subsec@@arg#1{\mysubsec[#1]}
\let\subsec@@noarg=\mysubsec

\def\EnDash{\leavevmode ─}

\let\ORIG@expldash=\expldash
\def\expldash{\ORIG@expldash}
\let\EmDash=\expldash

\clubpenalty=0\widowpenalty=0
\flushbottom

\AtBeginDocument{\vfuzz=10pt}
\parindent=1em

\RequirePackage{silence}
\WarningsOff*

\parskip=0pt plus 1pt minus 1pt

\ifx\thetitle\undefined\def\thetitle{}\fi


\endinput

%% Changes:
%% 1.0.11: let \paragraph equals \section*
%% 1.0.10: redefine \subsection to get normal argument
%% 1.0.9 : introduce linecommand
%% 1.0.8 : default font set to Nanum.
%% 1.0.7 : \title, \author, \date, \maketitle for lyx
%% 1.0.14: all font reverted to Noto Serif/sans

