latex config: pdfebook
Title: 간략한 사용법 개요
Author: The Author
Date: 10 Dec, 2021
base header level: 2
use xelatex: yes
ks prep: yes
automaketoc: no
---

# 이 스타일에 대하여

`mynovel`이라는 이 양식 파일은 아이패드에서 읽기 적당한 pdf를 생성하려는 목적으로
제작하였다.

## 실행에 필요한 파일 목록

다음과 같은 파일을 욲음으로 제공한다. 

  1. `mmd6-mynovel-*.tex`: leader, begin, footer 세 개의 파일이 있고, 그밖에 `mmd6-korean-common-leader.tex`이라는 파일이 있다.
  1. `multimarkdown.linux`: 실행 파일이다. Ubuntu Linux를 위한 것이므로 다른 운영체제라면 이를 현재 폴더에서 삭제한다.
  1. `mymmd2pdf`: 실행 스크립트이다. `md` 파일에 대하여 이 스크립트를 실행한다.
  1. `ks-mynovel.cls`: pdf를 생성하기 위하여 필요한 클래스 파일이다.
  
이 정도 규모의 파일을 별도로 설치할 필요는 없다고 생각한다. 작업 디렉터리에 두고 문서를 작성하라.
 
## multimarkdown의 설치에 관한 주석

[github.com/fletcher](https://github.com/fletcher/MultiMarkdown-6)로부터 소스를 받아서 컴파일하여도 좋지만, Mac에서는 homebrew로 설치할 수 있고 Windows에서는 실행 파일 바이너리를 다운로드받을 수 있다.
Linux의 경우에는 직접 컴파일하거나, Linuxbrew를 통하여 설치하여도 좋다. Linux가 제일 번거롭기 때문에 현재 우분투에서 실행되는 바이너리를 포함하였다.[^이 바이너리는 Windows나 Mac에서는 실행되지 않으므로 주의.]

윈도우즈를 위한 `mymmd2pdf` 스크립트의 배치파일 버전이나 실행파일은 제공하지 않는다. Windows의 WSL을 통하여 실행하는 것을 권장한다.[^윈도우즈용 multimarkdown 바이너리를 이용하고 이 스크립트를 쓰려 하는 때에는 스크립트 안의 `multimarkdown`에 `.exe` 확장자를 붙여 실행하라.]

# Markdown 문서의 작성

## YAML header

multimarkdown의 YAML header key들은 그대로 받아들인다. 그 중에 중요한 것은 다음과 같다.

base header level
:	이 값은 `2`로 고정해야 하고 다른 것은 쓰지 말 것. 그리고 이 key가 꼭 있어야 한다.

use xelatex
:	`\XeLaTeX`{=latex} 사용을 활성화. 원한다면 `use lualatex`도 가능하다.

latex config
:	이 양식은 이름이 `mynovel`이다.

title
:	제목. 보조 제목이 필요하거나 여러 줄 제목이 필요하다면 세미콜론(;)으로 구분하여 입력한다.

author
:	저자명. 여러 명의 저자를 여러 줄로 나열하려면 세미콜론(;)으로 구분하여 입력한다.

date
:	작성일자

이 양식 자체를 위한 key가 몇 가지 추가되어 있다.

ks prep
:	꼭 필요하다고 생각하면 되고 `yes`를 값으로 준다.

autotitlepage
:	표제지를 만드는 것을 지정한다. `yes` 또는 `no`. 별도로 선언하지 않으면 `yes`
상태가 기본이다.

automaketoc
:	목차를 만드는 것을 지정한다. 목차가 만들어지면 모든 페이지의 하단에 목차로의 바로가기가 붙는다. 기본값은 `yes`이고 별도로 지정하지 않아도 된다. 목차를 만들지
않을 때는 `no`를 줄 것.

titlepageimage
: 표제면의 배경 그림을 지정할 수 있다. 파일 이름을 직접 지정한다. 그림은 무조건 페이지 전체에 aspectratio를 유지하면서 배경으로 되므로, 필요하다면 미리 그림을 잘 만들어두어야 할 것이다.


## ks prep에 의한 확장

개행
:	(띄어쓰기 없이) `@ @`와 `@ @ @`에 의하여 개행을 강제 지정할 수 있다. `@ @`은 `\newline`을,
빈 줄 다음에 `@ @ @`은 `\bigskip`을 의미한다.

`$` 수식
:	multimarkdown 자체에서는 예컨대 수식을 다음과 같이 쓴다.

```
\\(a^2+b^2=c^2\\)
```

그 결과는 다음과 같다. \\(a^2+b^2=c^2\\)

이것이 불편하다고 생각하여 `$`를 이용한 입력을 가능하게 하였다. 
$a^2+b^2=c^2$

진짜로 \$ 기호 자체를 적으려면 백슬래시로 이스케이프한다.

\$\$로 디스플레이 수식을 적을 수 있다. multimarkdown의 방법인
`\\[`, `\\]`가 불편하지 않다면 그것을 써도 좋고, 이 \$\$ 자체는
multimarkdown이 이해할 것이다.

$$
\int_a^b f(x) dx= [F(x)]_a^b 
$$

여러 줄 수식은 예를 들면 다음과 같이 하면 된다.

```
$$
\begin{aligned}
\int_a^b f(x) dx &= \left[\mathstrut F(x)\right]_a^b \\
&= F(b)- F(a)
\end{aligned}
$$
```
$$
\begin{aligned}
\int_a^b f(x) dx &= \left[\mathstrut F(x)\right]_a^b \\
&= F(b)- F(a)
\end{aligned}
$$

수학 글꼴은 별달리 정의하지 않았다. 

# 간단한 multimarkdown markup의 보기

## 문자

```
글자 *글자* **글자** _글자_ __글자__
```

글자 *글자* **글자** _글자_ __글자__

emph 속성에서 기울어진 글자체를 쓰지 않는다. 그 대신 고딕체로 찍힌다.

## 문단

````
> 인용문단. 일찍 일어나는 새가 피곤하다.
````

> 인용문단. 일찍 일어나는 새가 피곤하다.

````
* 리스트 문단
* 리스트
````

  * 리스트 문단
  * 리스트

````
1. 번호붙인 리스트
1. 다시 번호 붙인 리스트
````

1. 번호붙인 리스트
1. 다시 번호 붙인 리스트

````
설명 문단
:	설명 문단이란, 설명을 하기 위한 문단이다.
````

설명 문단
:	설명 문단이란, 설명을 하기 위한 문단이다.

````
각주[^이것은 주석이다]
````

각주[^이것은 주석이다]

@@@

각주를 다는 복잡한 방법이 있지만 생략한다.

## Verbation

````
문단 중에서 인라인 `verbatim`은 백쿼트 문자를 하나만 쓴다.
````

문단 중에서 인라인 `verbatim`은 백쿼트 문자를 하나만 쓴다.

````
```
별행 문단인 verbatim은
세 개의 백쿼트 문자를 쓴다.
```
````

```
별행 문단인 verbatim은
세 개의 백쿼트 문자를 쓴다.
```

소스는 다음과 같은 방식으로 보인다.

````
```
\documentclass{article}
\beign{document}
Hello, world!
\end{document}
```
````

`\LaTeX{}`{=latex} 코드를 넣으려면 다음과 같이 한다. 이 기능은 문서 중간에 `\LaTeX{}`{=latex} 코드를 실행하고 싶을 때 쓸 수 있다. 예를 들면,

````
`\LaTeX`{=latex}
````

이것은 `\LaTeX`{=latex}으로 찍힌다.

이 양식은 다른 언어는 지원하지 않는다. 전문적인 소스코드 리스팅을 하기 위해서라면
이 *pdfebook* 포맷이 아니라 그를 위해 준비된 포맷을 사용하여야 할 것이다.

## url 

인터넷 주소를 입력하려면 

````
[KTUG](http://ktug.org)
````

[KTUG](http://ktug.org)


## 이미지

그림은 multimarkdown 문법으로 다음과 같이 넣을 수 있다.

````
![myimage](example-image.jpg width=5cm)
````

![myimage](example-image.jpg width=5cm)

이 결과는 `figure` 환경에 넣는 것이다. multimarkdown은 그림이 문장 중에 있지 않고 별도의 문단으로 마크업되면 figure 환경 안에 넣는다. 다만 label이 붙지 않는데, label은 상호참조를 위한 것이다.

글 중간에서는 그냥 그림으로 들어온다. ![](example-image.jpg height=15pt) 이런 식으로.

multimarkdown의 reference 기능을 이용하면 더 우아하게 할 수 있다. 보기만 보이겠다.

````
그림 ![newimg][]를 넣어 보았다.

[newimg]: example-image.jpg width=2cm
````

그림 ![newimg][]를 넣어 보았다.

[newimg]: example-image.jpg width=2cm

# 마침

이 정도면 필요한 것은 거의 다 알았다고 할 수 있다. 본격적인 글쓰기를 위한
포맷, 예컨대 `mmd6-oblivoir` 같은 것과 달리 이 pdfebook은 텍본을
pdf로 만드는 데 중점을 두고 있어서 간략화한 것이 매우 많다.

`\clearpage\tableofcontents*`{=latex}
