#!/bin/sh

file_name=`echo $1| sed 's/\.[^.]*$//'`

chklatex=`multimarkdown -e latexconfig "$1"`

if [ "$chklatex" != "" ]
then
	cp "$1" "$file_name.@md"
else
	cat `kpsewhich common_header.txt` "$1" > "$file_name.@md"
fi

mymmd2pdf "$file_name.@md"

rm -f "$file_name.@md"
